/**
 * The batterfield is here.
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */

public class BattleOfBastards {
    public static void main(String[] args) {

        // Manual dependencies injection
        // Stark stark = new Stark();
        // Bolton bolton = new Bolton();

        // War war = new War(stark, bolton);
        // war.prepare();
        // war.report();

        // Using Dagger 2

        // This is because, adding a module, we need to pass the module dependency to
        // the dagger. This we need to pass it like this.

        // BattleComponent component = DaggerBattleComponent
        // .builder().braavosModule(new BraavosModule(cash, soldiers)).build();

        // After including all the modules, you can start using the methods using the
        // component.

        Cash cash = new Cash();
        Soldiers soldiers = new Soldiers();
        BattleComponent battleComponent = DaggerBattleComponent.builder()
                .braavosModule(new BraavosModule(cash, soldiers)).build();
        War war = battleComponent.getWar();
        war.prepare();
        war.report();

        battleComponent.getCash();
        battleComponent.getSoldiers();
    }
}