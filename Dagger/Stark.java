/**
 * The Stark is ready for wars.
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */
public class Stark implements House{

    private static final String NAME = Stark.class.getSimpleName();

    // Field injection
    @Inject
    Allies allies;

    // Constructor injection -- Dagger 2
    @Inject 
    public Stark(){
        // do something 
    }

    // Method injection
    @Inject
    @Override
    public void prepareWar() {
        System.out.println(NAME + " prepared for wars");
    }

    // Method injection
    @Inject
    @Override
    public void reportWar() {
        System.out.println(NAME + " is reporting ...");
    }
}