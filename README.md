# Injection dependency's tutorial

## Principe

In the simple way of injection dependency, no class should instantiate another class but should get the instances from a configuration class. They respect the concept of Inversion of Control which say that a class should get its dependencies fromn outside.

In an application that relies on dependency injection, the objects never have to hunt around for dependencies or construct themselves. All the dependencies are provided or injected so that they are ready to be used.

## @Inject

- Constructor Injection — is used with class constructor
- Field Injection — is used with the fields in the class
- Method Injection — is used with the functions/methods

In other words, the @Inject annotation will tell the Dagger what all the dependencies needed to be transferred to the dependant

## @Component

This annotation is used to build the interface which wires everything together. In this place, we define from which modules (or other Components) we’re taking dependencies. Also here is the place to define which graph dependencies should be visible publicly (can be injected) and where our component can inject objects. @Component, in general, is a something like a bridge between @Module(Which we’ll be seeing later) and @Inject.

In other words, @Component annotations are the agent who is in charge of approving and transferring the loan amount to the respective accounts in the iron bank.

## @Module

In short, @Module annotation, marks the modules/classes. For example, Let’s talk in Android. We can have a module called ContextModule and this module can provide ApplicationContext and Context dependencies to other classes. So we need to mark the class ContextModule with @Module.

## @Provides

In short, @Provides annotation marks the methods inside the modules, which provides the dependencies. For example, in the same example above, we marked ContextModule with @Module and we need to mark methods which provide ApplicationContext and Context with @Provides.