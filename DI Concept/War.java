/**
 * Let's the battle of bastard begin. In this class, inside the constructor,
 * they depend upon 2 objects Stark and Bolton to bring the war. To respect the
 * concept of Inversion of Control, the class sould not create or have its
 * dependencies inside. So, we have to get its from outside.
 *
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */
public class War {
    private Stark stark;
    private Bolton bolton;

    // DI - getting dependencies from outside via constructor
    public War(Stark stark, Bolton bolton) {
        this.stark = stark;
        this.bolton = bolton;
    }

    public void prepare() {
        stark.prepareWar();
        bolton.prepareWar();
    }

    public void report(){
        stark.reportWar();
        bolton.reportWar;
    }
}