/**
 * The batterfield is here.
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */

public class BattleOfBastards {
    public static void main(String[] args) {
        Stark stark = new Stark();
        Bolton bolton = new Bolton();

        War war = new War(stark, bolton);
        war.prepare();
        war.report();
    }
}