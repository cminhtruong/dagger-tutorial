/**
 * To begin the battle of bastard, the definition of house is required with 2 methods which will bring the war to them.
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */

public interface House {
    void prepareWar();

    void reportWar();
}