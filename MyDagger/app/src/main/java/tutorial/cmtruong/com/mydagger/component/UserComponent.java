package tutorial.cmtruong.com.mydagger.component;

import com.squareup.picasso.Picasso;

import dagger.Component;
import tutorial.cmtruong.com.mydagger.module.PicassoModule;
import tutorial.cmtruong.com.mydagger.module.UserModule;
import tutorial.cmtruong.com.mydagger.service.APIService;

/**
 * 1st step: create a component
 */

@Component(modules = {UserModule.class, PicassoModule.class})
public interface UserComponent {

    APIService getUser();

    Picasso getPicasso();
}
