package tutorial.cmtruong.com.mydagger.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import tutorial.cmtruong.com.mydagger.model.RandomUsers;

public interface APIService {

    @GET("api")
    Call<RandomUsers> getRandomUsers(@Query("results") int size);
}
