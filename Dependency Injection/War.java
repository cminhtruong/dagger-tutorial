/**
 * Let's the battle of bastard begin.
 * In this class, inside the constructor, they depend upon 2 objects Stark and Bolton to bring the war. 
 * So, they are the dependencies of this class. Without them, we cannot be
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 *        
 */
public class War {
    private Stark stark;
    private Bolton bolton;

    public War() {

        // Instantiate 2 houses
        stark = new Stark();
        bolton = new Bolton();

        // The battle is prepared
        stark.prepareWar();
        bolton.prepareWar();

        // 2 houses are reporting
        stark.reportWar();
        bolton.reportWar();
    }
}