/**
 * The Stark is ready for wars.
 * 
 * @author Davide Truong
 * @version 1.0
 * @since July 11th, 2018
 */
public class Stark implements House {

    private static final String NAME = Stark.class.getSimpleName();

    @Override
    public void prepareWar() {
        System.out.println(NAME + " prepared for wars");
    }

    @Override
    public void reportWar() {
        System.out.println(NAME + " is reporting ...");
    }
}